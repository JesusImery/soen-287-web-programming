-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2015 at 08:22 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rental_7174276`
--
CREATE DATABASE IF NOT EXISTS `rental_7174276` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `rental_7174276`;

-- --------------------------------------------------------

--
-- Table structure for table `ownerslisting`
--

CREATE TABLE IF NOT EXISTS `ownerslisting` (
`id` int(11) NOT NULL,
  `login` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `price` varchar(20) NOT NULL,
  `availability` varchar(50) NOT NULL,
  `message` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ownerslisting`
--

INSERT INTO `ownerslisting` (`id`, `login`, `title`, `location`, `price`, `availability`, `message`) VALUES
(1, 'JesusImery', 'asd', 'Street,City,Postal Code, Province', 'asd', 'now', 'asd'),
(2, 'JesusImery', 'asd', 'Street,City,Postal Code, Province', 'ads', 'now', 'asdasd');

-- --------------------------------------------------------

--
-- Table structure for table `ownersregistration`
--

CREATE TABLE IF NOT EXISTS `ownersregistration` (
`id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `sname` varchar(100) NOT NULL,
  `phone` varchar(13) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `login` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ownersregistration`
--

INSERT INTO `ownersregistration` (`id`, `fname`, `sname`, `phone`, `email`, `login`, `password`) VALUES
(1, 'Jesus', 'Imery', '(514)555-3333', 'yisas_0929@hotmail.com', 'JesusImery', 'jesusimery1'),
(2, 'David', 'Ochoa', '(514)555-4444', 'davidochoa@gmail.com', 'Dochoa', 'david01'),
(3, 'isa', 'asd', '(514)555-3333', 'yisas_0929@hotmail.com', 'kasdasd', 'asd123'),
(4, 'asdasd', 'asdasdsad', '(514)555-3333', 'yisas_0929@hotmail.com', 'kas123', 'asd123');

-- --------------------------------------------------------

--
-- Table structure for table `ownersselectioncriteria`
--

CREATE TABLE IF NOT EXISTS `ownersselectioncriteria` (
`id` int(11) NOT NULL,
  `login` varchar(100) NOT NULL,
  `pets` varchar(3) NOT NULL,
  `smoking` varchar(3) NOT NULL,
  `age` varchar(3) NOT NULL,
  `occupation` varchar(13) NOT NULL,
  `income` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ownersselectioncriteria`
--

INSERT INTO `ownersselectioncriteria` (`id`, `login`, `pets`, `smoking`, `age`, `occupation`, `income`) VALUES
(1, 'JesusImery', 'no', 'no', '182', 'student', '<1000 moth per month');

-- --------------------------------------------------------

--
-- Table structure for table `tenantspreference`
--

CREATE TABLE IF NOT EXISTS `tenantspreference` (
`id` int(11) NOT NULL,
  `login` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `street` varchar(100) DEFAULT NULL,
  `province` varchar(100) NOT NULL,
  `pricerange` varchar(100) NOT NULL,
  `availability` varchar(100) NOT NULL,
  `message` varchar(300) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tenantspreference`
--

INSERT INTO `tenantspreference` (`id`, `login`, `city`, `street`, `province`, `pricerange`, `availability`, `message`) VALUES
(9, 'Isabella', 'asd', 'asdasd', 'asdasd', '500', 'now', 'Owners should be able to post their tenant selection criteria to the two-sided matching market,\r\nso that they may be matched with tenants who best suit their criteria. For example, an owner\r\nmay choose to select their tenant based on whether the tenant has a pet, smoking vs. nonsmoking,\r\nage of tena');

-- --------------------------------------------------------

--
-- Table structure for table `tenantsprofile`
--

CREATE TABLE IF NOT EXISTS `tenantsprofile` (
`id` int(11) NOT NULL,
  `login` varchar(100) NOT NULL,
  `pets` varchar(3) NOT NULL,
  `smoking` varchar(3) NOT NULL,
  `age` varchar(3) NOT NULL,
  `occupation` varchar(13) NOT NULL,
  `income` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tenantsprofile`
--

INSERT INTO `tenantsprofile` (`id`, `login`, `pets`, `smoking`, `age`, `occupation`, `income`) VALUES
(10, 'Isabella', 'no', 'no', '12', 'student', '<1000 moth per month');

-- --------------------------------------------------------

--
-- Table structure for table `tenantsregistration`
--

CREATE TABLE IF NOT EXISTS `tenantsregistration` (
`id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `sname` varchar(100) NOT NULL,
  `phone` varchar(13) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `login` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tenantsregistration`
--

INSERT INTO `tenantsregistration` (`id`, `fname`, `sname`, `phone`, `email`, `login`, `password`) VALUES
(1, 'Isabella', 'Rubial', '(514)444-3333', 'isabellarubialc@gmail.com', 'Isabella', 'isabella123'),
(2, 'David', 'Orswell', '(514)222-3333', 'dowrswell@gmail.com', 'Dorswell', '123asd'),
(3, 'Isabella', 'Rubial', '(514)222-3333', 'isabellarubial01@gmail.com', 'Isabella', 'isabella123'),
(4, 'Isabella', 'Rubial', '(514)222-3333', 'isabellarubial01@gmail.com', 'Isabella', 'isabella123'),
(5, 'sad', 'asd', '(514)555-3333', 'yisas_0929@hotmail.com', 'Isabella', 'asd123123'),
(6, 'Isabella', 'Rubial', '(514)555-3333', 'isabellarubial43@gmail.com', 'Isabella', 'isabella123123'),
(7, 'Jesus', 'asd', '(514)555-3333', 'isabellarubial01@gmail.com', 'Isabella', 'isabella12'),
(8, 'Isabella', 'asd', '(514)222-3333', 'yisas_0929@hotmail.com', 'Isabella', 'asd123'),
(9, 'Isabella', 'asd', '(514)555-3333', 'yisas_0929@hotmail.com', 'Isabella', 'asd123123'),
(10, 'asd', 'asdasd', '(514)555-3333', 'yisas_0929@hotmail.com', 'Isabella', 'asd123'),
(11, 'Isabella', 'jasdas', '(514)555-3333', 'yisas_0929@hotmail.com', 'Isabella', 'isabella123'),
(12, 'Isabella', 'jasdjasd', '(514)555-3333', 'yisas_0929@hotmail.com', 'Isabella', 'isa123'),
(13, 'sd', 'asd', '(514)555-3333', 'yisas_0929@hotmail.com', 'Isabella', 'asd123'),
(14, 'adsasdasd', 'ads', '(514)555-3333', 'yisas_0929@hotmail.com', 'Isabella', 'ka1234'),
(15, 'Isabella', 'ads', '(514)555-3333', 'yisas_0929@hotmail.com', 'Isabella', 'isa123'),
(16, 'Isabella', 'sda', '(514)555-3333', 'yisas_0929@hotmail.com', 'Isabella', 'asd123'),
(17, 'Isabella', 'Rubial', '(514)555-3333', 'yisas_0929@hotmail.com', 'kasdasd', 'asd123'),
(18, 'Isabella', 'asd', '(514)555-3333', 'yisas_0929@hotmail.com', 'jasdasd', 'asd123');

-- --------------------------------------------------------

--
-- Table structure for table `userreginfo`
--

CREATE TABLE IF NOT EXISTS `userreginfo` (
`id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lnam` varchar(100) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `email` varchar(100) NOT NULL,
  `login` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userreginfo`
--

INSERT INTO `userreginfo` (`id`, `fname`, `lnam`, `phone`, `email`, `login`, `password`) VALUES
(1, 'Jesus', 'Imery', '514-5555555', 'yisas_0929@hotmail.com', 'Jesus', 'jesus123'),
(2, '', '', '', '', '', ''),
(3, '', '', '', '', '', ''),
(4, '', '', '', '', '', ''),
(5, '', '', '', '', '', ''),
(6, '', '', '', '', '', ''),
(7, '', '', '', '', '', ''),
(8, 'asdasd', '', '', '', '', ''),
(9, 'asdasd', '', '', '', '', ''),
(10, 'asd', 'asdasdsad', '', '', '', ''),
(11, 'asdasd', 'sadnasd', '', '', '', ''),
(12, 'adsasd', 'asdasdasdas', '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ownerslisting`
--
ALTER TABLE `ownerslisting`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ownersregistration`
--
ALTER TABLE `ownersregistration`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ownersselectioncriteria`
--
ALTER TABLE `ownersselectioncriteria`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenantspreference`
--
ALTER TABLE `tenantspreference`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenantsprofile`
--
ALTER TABLE `tenantsprofile`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenantsregistration`
--
ALTER TABLE `tenantsregistration`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userreginfo`
--
ALTER TABLE `userreginfo`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ownerslisting`
--
ALTER TABLE `ownerslisting`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ownersregistration`
--
ALTER TABLE `ownersregistration`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ownersselectioncriteria`
--
ALTER TABLE `ownersselectioncriteria`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tenantspreference`
--
ALTER TABLE `tenantspreference`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tenantsprofile`
--
ALTER TABLE `tenantsprofile`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tenantsregistration`
--
ALTER TABLE `tenantsregistration`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `userreginfo`
--
ALTER TABLE `userreginfo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
