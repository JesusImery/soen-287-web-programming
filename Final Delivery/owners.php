<!DOCTYPE html>
<html>
<head>
<title>Rental Matching Market - Owners</title>
<meta name="author" content="Jesus Imery">
<meta name="keywords" content="rent,match,tenant,property,owner,lease">
<link rel="stylesheet" type="text/css" href="rentalmatchingmarket.css">
<script src="rentalmatchingmarket.js"></script>
<?php
session_start();
$login=$_SESSION["sessionlogin"];
$rebuildListing=false;
$Err = "";

function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

function appendRowWithData($data, $type){
echo '
var tr1 = document.createElement("TR");
var td1 = document.createElement("TD");';
echo 'var datatype = "' . $type . '";';
echo '
var t2 = document.createTextNode(datatype);
td1.appendChild(t2);
tr1.appendChild(td1);
var td1 = document.createElement("TD");
';
echo 'var data = ' . $data . ';
var t2 = document.createTextNode(data);
td1.appendChild(t2);
tr1.appendChild(td1);
document.getElementById("myTable").appendChild(tr1);
';
}

if(isset($_POST['selectionCriteriaSubmit']))
{
$dbhost = 'localhost:3306';
$dbuser = 'root';
$dbpass = '';
$dbname = 'rental_7174276';
$conn = new mysqli($dbhost, $dbuser, $dbpass,$dbname);
if(! $conn )
{
  die('Could not connect: ' . mysql_error());
}

if(! get_magic_quotes_gpc() )
{
   $pets = addslashes ($_POST['pets']);
   $smoking = addslashes ($_POST['smoking']);
   $age = addslashes ($_POST['age']);
   $occupation = addslashes ($_POST['occupation']);
   $income = addslashes ($_POST['income']);
}
else
{
      $pets =  ($_POST['pets']);
   $smoking =  ($_POST['smoking']);
   $age =  ($_POST['age']);
   $occupation =  ($_POST['occupation']);
   $income =  ($_POST['income']);
}


	  $sql = "SELECT id, login FROM ownersselectioncriteria WHERE login='" . $login . "'";
		$result = $conn->query($sql);

  if ($result->num_rows > 0) {
    
echo '<script language="javascript">';
echo 'alert("Overwriting Selection Criteria!")';
echo '</script>';

	$sql= "DELETE FROM ownersselectioncriteria WHERE login='" . $login . "'";
	$retval = mysqli_query( $conn, $sql );
	if(! $retval )
{
  die('Could not enter data: ' . mysql_error());
}
	}



	$sql = "INSERT INTO  ownersselectioncriteria".
       "(login,pets,smoking,age,occupation,income)".
       "VALUES ".
       "('$login','$pets','$smoking','$age','$occupation','$income')";
	   mysql_select_db('rental_7174276');
$retval = mysqli_query(  $conn, $sql);
if(! $retval )
{
  die('Could not enter data: ' . mysql_error());
}
echo '<script language="javascript">';
echo 'alert("Data entered succesfully!")';
echo '</script>';
mysqli_close($conn);
goto pag;

}
else
{
if (isset($_POST['listingSubmit']))
{
$dbhost = 'localhost:3306';
$dbuser = 'root';
$dbpass = '';
$dbname = 'rental_7174276';
$conn = new mysqli($dbhost, $dbuser, $dbpass,$dbname);
if(! $conn )
{
  die('Could not connect: ' . mysql_error());
}

if(! get_magic_quotes_gpc() )
{

  $title = addslashes ($_POST['title']);
   $location = addslashes ($_POST['location']);
   $price = addslashes ($_POST['price']);
   $availability = addslashes ($_POST['availability']);
   $message = addslashes ($_POST['message']);
}
else
{
      $title =  ($_POST['title']);
   $location =  ($_POST['location']);
   $price =  ($_POST['price']);
   $availability =  ($_POST['availability']);
   $message =  ($_POST['message']);
}

$result=true;

if (empty($_POST["title"])) {
     $Err = $Err . "Title is required. ";
	 $result=false;
   }

   if (empty($_POST["location"])) {
     $Err = $Err . "Location is required. ";
	 $result=false;
   }

      if (empty($_POST["price"])) {
     $Err = $Err . "Price is required. ";
	 $result=false;
   }


    if ($result==true)
	{
	$login=$_SESSION["sessionlogin"];


	$sql = "INSERT INTO  ownerslisting".
       "(login,title,location,price,availability,message)".
       "VALUES ".
       "('$login','$title','$location','$price','$availability','$message')";
	   mysql_select_db('rental_7174276');
$retval = mysqli_query(  $conn, $sql);
if(! $retval )
{
  die('Could not enter data: ' . mysql_error());
}
echo '<script language="javascript">';
echo 'alert("Data entered succesfully!")';
echo '</script>';
mysqli_close($conn);
goto pag;
}
else
{
$rebuildListing=true;
goto pag;
}
}
else
{
pag:
?>
</head>
<body>

<div class="top" id="top">
<h1>Owner's Menu</h1>
<center>
<table>
<tr>
<th>
<a href='index.html' class='button'>Home</a>
</th>
<th>
<a href='register.php' class='button'>Register</a></th>
<th>
<a href='ownerslogin.php' class='button'>Owners</a></th>
<th>
<a href='tenantslogin.php' class='button'>Tenants</a></th>
</tr>
</table>
</center>
</div>

<div class="left">
<p class='button' onclick="createListing()">List a Property for Rent</></p>
    <p class='button' onclick="setTenantSelectionCriteria()">Set Tenant Selection Criteria</></p>
	    <p class='button' onclick='location.href="?viewProperty=1"'>View Property</></p>
			    <p class='button' onclick='location.href="?viewSelectionCriteria=1"'>View Rental Selection Criteria</></p>


</div>
 
    <div class="main">
        <center>
		<p id="welcome">Welcome <?php echo $_SESSION["sessionfname"] . "!"; ?></p>
		<p id="mainBody">Select an option to start</p>
	<span class="error"><?php echo $Err;?></span>
		</center>
              </div>
     <footer class="fixed">
<p>Copyright: Imaginary INC.</p>
<p><a href="mailto:yisas_0929@hotmail.com">Contact: yisas_0929@hotmail.com</a></p>
</footer>
<?php
if ($rebuildListing==true)
{
  echo '<script language="javascript"> createListing();  </script>';
}
}
}
if(isset ($_GET['viewProperty']))
{
$dbhost = 'localhost:3306';
$dbuser = 'root';
$dbpass = '';
$dbname = 'rental_7174276';
$conn = new mysqli($dbhost, $dbuser, $dbpass,$dbname);
if(! $conn )
{
  die('Could not connect: ' . mysql_error());
}
 $sql = "SELECT * FROM ownerslisting WHERE login='" . $login . "'";
	mysql_select_db('rental_7174276');
	$result = $conn->query($sql);
  if ($result->num_rows == 0) {
echo '<script language="javascript">';
echo 'alert("No listings yet!")';
echo '</script>';
$result2=false;
}
else
{
$row=$result->fetch_assoc();
 $title = json_encode($row["title"]);
  $location = json_encode($row["location"]);
    $price = json_encode($row["price"]);
	    $availability = json_encode($row["availability"]);
			    $message = json_encode($row["message"]);



echo '<script language="javascript">';
echo '
document.getElementById("mainBody").innerHTML = "";
document.getElementById("welcome").innerHTML = "";
var table = document.createElement("TABLE");
table.setAttribute("id", "myTable");
table.setAttribute("class","backgroundBox");
document.getElementById("mainBody").appendChild(table);
';
 $message="title:";
echo ''. appendRowWithData($title,$message) .'
';
 $message="location:";
echo ''. appendRowWithData($location,$message) .'
';
 $message="Age:";
echo ''. appendRowWithData($age,$message) .'
';
 $message="availability:";
echo ''. appendRowWithData($availability,$message) .'
';
 $message2="message:";
echo ''. appendRowWithData($message,$message2) .'
</script>';
}
}

if(isset ($_GET['viewSelectionCriteria']))
{
$dbhost = 'localhost:3306';
$dbuser = 'root';
$dbpass = '';
$dbname = 'rental_7174276';
$conn = new mysqli($dbhost, $dbuser, $dbpass,$dbname);
if(! $conn )
{
  die('Could not connect: ' . mysql_error());
}
 $sql = "SELECT * FROM ownersselectioncriteria WHERE login='" . $login . "'";
	mysql_select_db('rental_7174276');
	$result = $conn->query($sql);
  if ($result->num_rows == 0) {
echo '<script language="javascript">';
echo 'alert("No Selection Criteria yet!")';
echo '</script>';
$result2=false;
}
else
{
$row=$result->fetch_assoc();
 $pets = json_encode($row["pets"]);
  $smoking = json_encode($row["smoking"]);
    $age = json_encode($row["age"]);
			    $occupation = json_encode($row["occupation"]);
							    $income = json_encode($row["income"]);




echo '<script language="javascript">';
echo '
document.getElementById("mainBody").innerHTML = "";
document.getElementById("welcome").innerHTML = "";
var table = document.createElement("TABLE");
table.setAttribute("id", "myTable");
table.setAttribute("class","backgroundBox");
document.getElementById("mainBody").appendChild(table);
';
 $message="Pets:";
echo ''. appendRowWithData($pets,$message) .'
';
 $message="Smoking:";
echo ''. appendRowWithData($smoking,$message) .'
';
 $message="Age:";
echo ''. appendRowWithData($age,$message) .'
';
 $message="Occupation:";
echo ''. appendRowWithData($occupation,$message) .'
';
 $message="Income:";
echo ''. appendRowWithData($income,$message) .'
</script>';
}
}
?>
</body>
</html>

