function redirectToOwners() {
    if (typeof (Storage) !== "undefined") 
        if (localStorage.ownersSession)
            window.location.assign("owners.html");
}

function redirectToTenants() {
    if (typeof (Storage) !== "undefined")
        if (localStorage.tenatsSession)
            window.location.assign("tenants.html");
}

function validateRegistration() {
    var result = true;
var namepatt = new RegExp(/^[A-Za-z\-]+$/);
var firstname = document.getElementById("firstname").value;
document.getElementById("responsepar").innerHTML = "";
if (!namepatt.test(firstname)) {
    document.getElementById("responsepar").innerHTML += "First name invalid.\n";
    result = false;
}
var secondname=document.getElementById("secondname").value;
if (!namepatt.test(secondname)) {
    document.getElementById("responsepar").innerHTML += "Second name invalid.\n";
    result = false;
}
var phone = document.getElementById("phonenumber").value;
var phonepatt = new RegExp(/^\([0-9]{3}\)[0-9]{3}\-[0-9]{4}$/);
if (!phonepatt.test(phone)) {
    document.getElementById("responsepar").innerHTML += "Phone number invalid.\n";
    result = false;
}
var email = document.getElementById("emailaddress").value;
var emailpatt = new RegExp(/^[A-Za-z0-9_]+@[A-Za-z0-9]+\.com$/);
if (!emailpatt.test(email)) {
    document.getElementById("responsepar").innerHTML += "Email address invalid.\n";
    result = false;
}
var login = document.getElementById("login").value;
var loginpatt = new RegExp(/^[A-Za-z0-9]{6}[A-Za-z0-9]*$/);
if (!loginpatt.test(login)) {
    document.getElementById("responsepar").innerHTML += "Login name invalid.\n";
    result = false;
}
var pass1 = document.getElementById("loginpassword").value;
var pass2 = document.getElementById("confirmpassword").value;
var passpatt = new RegExp(/^.*(?=.{6,})(?=.*\d)(?=.*[a-zA-Z]).*$/);
if (!passpatt.test(pass1)) {
    document.getElementById("responsepar").innerHTML += "Password invalid.\n";
    result = false;
}
if (pass1 != pass2) {
    document.getElementById("responsepar").innerHTML += "Passwords don't match.\n";
    result = false;
}
if (result)
    document.getElementById("responsepar").innerHTML= "Registration Succesful!";

}


function validateOwnersLogin() {
    document.getElementById("responsepar").innerHTML = "";
    var result = true;
    var login = document.getElementById("login").value;
    var loginpatt = new RegExp(/^[A-Za-z0-9]{6}[A-Za-z0-9]*$/);
    if (!loginpatt.test(login)) {
        document.getElementById("responsepar").innerHTML += "Login name invalid.\n";
        result = false;
    }
    var pass1 = document.getElementById("loginpassword").value;
    var passpatt = new RegExp(/^.*(?=.{6,})(?=.*\d)(?=.*[a-zA-Z]).*$/);
    if (!passpatt.test(pass1)) {
        document.getElementById("responsepar").innerHTML += "Password invalid.\n";
        result = false;
    }
    if (result) {
        localStorage.ownersSession = 1;
        window.location.assign("owners.html");
    }

}


function validateTenantsLogin() {
    document.getElementById("responsepar").innerHTML = "";
    var result = true;
    var login = document.getElementById("login").value;
    var loginpatt = new RegExp(/^[A-Za-z0-9]{6}[A-Za-z0-9]*$/);
    if (!loginpatt.test(login)) {
        document.getElementById("responsepar").innerHTML += "Login name invalid.\n";
        result = false;
    }
    var pass1 = document.getElementById("loginpassword").value;
    var passpatt = new RegExp(/^.*(?=.{6,})(?=.*\d)(?=.*[a-zA-Z]).*$/);
    if (!passpatt.test(pass1)) {
        document.getElementById("responsepar").innerHTML += "Password invalid.\n";
        result = false;
    }
    if (result) {
        localStorage.tenatsSession = 1;
        window.location.assign("tenants.html");
    }
}

function createListing() {
    document.getElementById("mainBody").innerHTML = "";
    // document.getElementById("welcome").innerHTML = "";
    var listingForm = document.createElement("FORM");
    listingForm.setAttribute("id", "listingForm");
    listingForm.setAttribute("name", "listingForm");
    listingForm.setAttribute("method", "post");
    listingForm.setAttribute("action", "owners.php");
    document.getElementById("mainBody").appendChild(listingForm);
    var table = document.createElement("TABLE");
    table.setAttribute("id", "myTable");

    document.getElementById("listingForm").appendChild(table);

    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");
    
    var t = document.createTextNode("Title:");
    td1.appendChild(t);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y2 = document.createElement("INPUT");

    y2.setAttribute("name", "title");

    y2.setAttribute("size", "60");

    td2.appendChild(y2);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);




    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Location of the Property:");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y2 = document.createElement("INPUT");

    y2.setAttribute("name", "location");

    y2.setAttribute("value", "Street,City,Postal Code, Province");

    y2.setAttribute("size","60");

    td2.appendChild(y2);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);



    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Price:");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y2 = document.createElement("INPUT");

    y2.setAttribute("size", "60");

    y2.setAttribute("name", "price");

    td2.appendChild(y2);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);



    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Availability:");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y5 = document.createElement("SELECT");

    y5.setAttribute("name", "availability");

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "now");
    var t5 = document.createTextNode("Available Now");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "nextmoth");
    var t5 = document.createTextNode("Available Next Month");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "message");
    var t5 = document.createTextNode("Availablity Specified with Description");
    opt.appendChild(t5);
    y5.appendChild(opt);

    td2.appendChild(y5);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);

    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Message:");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y4 = document.createElement("TEXTAREA");

    y4.setAttribute("name", "message");


    y4.setAttribute("rows", "10");
    y4.setAttribute("cols", "50");
    y4.setAttribute("maxlength", "300");

    td2.appendChild(y4);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);

    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    td1.setAttribute("colspan", "2");

    td1.setAttribute("align", "center");

    var y2 = document.createElement("INPUT");

    y2.setAttribute("type", "submit");

    y2.setAttribute("value", "Submit");

    y2.setAttribute("name", "listingSubmit");


    td1.appendChild(y2);

    tr1.appendChild(td1);

    document.getElementById("myTable").appendChild(tr1);
}

function setTenantSelectionCriteria() {
       document.getElementById("mainBody").innerHTML = "";
   // document.getElementById("welcome").innerHTML = "";
    var tenantSelectionForm = document.createElement("FORM");
    tenantSelectionForm.setAttribute("id", "tenantSelectionForm");
    tenantSelectionForm.setAttribute("name", "tenantSelectionForm");
    tenantSelectionForm.setAttribute("method", "post");
    tenantSelectionForm.setAttribute("action", "owners.php");
    document.getElementById("mainBody").appendChild(tenantSelectionForm);
    var table = document.createElement("TABLE");
    table.setAttribute("id", "myTable");


    document.getElementById("tenantSelectionForm").appendChild(table);


    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Has pets:");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y5 = document.createElement("SELECT");
    y5.setAttribute("name", "pets");

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "no");
    var t5 = document.createTextNode("No");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "yes");
    var t5 = document.createTextNode("Yes");
    opt.appendChild(t5);
    y5.appendChild(opt);

    td2.appendChild(y5);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);



    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Smoking:");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y5 = document.createElement("SELECT");
    y5.setAttribute("name", "smoking");


    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "no");
    var t5 = document.createTextNode("No");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "yes");
    var t5 = document.createTextNode("Yes");
    opt.appendChild(t5);
    y5.appendChild(opt);

    td2.appendChild(y5);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);

    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Age Range:");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y5 = document.createElement("SELECT");
    y5.setAttribute("name", "age");


    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "18-24");
    var t5 = document.createTextNode("18-24");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "25-30");
    var t5 = document.createTextNode("25-30");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "31-45");
    var t5 = document.createTextNode("31-45");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", ">45");
    var t5 = document.createTextNode(">45");
    opt.appendChild(t5);
    y5.appendChild(opt);

    td2.appendChild(y5);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);


    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Occupation:");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y5 = document.createElement("SELECT");
    y5.setAttribute("name", "occupation");

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "student");
    var t5 = document.createTextNode("Student");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "software");
    var t5 = document.createTextNode("Software");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "accounting");
    var t5 = document.createTextNode("Accounting");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "buisness");
    var t5 = document.createTextNode("Buisness");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "healthcare");
    var t5 = document.createTextNode("Healthcare");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "unemployed");
    var t5 = document.createTextNode("Unemployed");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "other");
    var t5 = document.createTextNode("Other");
    opt.appendChild(t5);
    y5.appendChild(opt);

    td2.appendChild(y5);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);


    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Level of Income:");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y5 = document.createElement("SELECT");

    y5.setAttribute("name", "income");

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "<1000 moth per month");
    var t5 = document.createTextNode("<1000 moth per month");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "1000-2000 per month");
    var t5 = document.createTextNode("1000-2000 per month");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "2000-5000 per month");
    var t5 = document.createTextNode("2000-5000 per month");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "5000-100000 per month");
    var t5 = document.createTextNode("5000-100000 per month");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "10000-20000 per month");
    var t5 = document.createTextNode("10000-20000 per month");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", ">20000 per moth");
    var t5 = document.createTextNode(">20000 per moth");
    opt.appendChild(t5);
    y5.appendChild(opt);

    td2.appendChild(y5);

    tr1.appendChild(td2);



    document.getElementById("myTable").appendChild(tr1);


    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    td1.setAttribute("colspan", "2");

    td1.setAttribute("align", "center");

    var y2 = document.createElement("INPUT");

    y2.setAttribute("type", "submit");

    y2.setAttribute("name", "selectionCriteriaSubmit");

    y2.setAttribute("value", "Submit");

    td1.appendChild(y2);

    tr1.appendChild(td1);

    document.getElementById("myTable").appendChild(tr1);
}

function setTenantProfile() {
    document.getElementById("mainBody").innerHTML = "";
    document.getElementById("welcome").innerHTML = "";
    var tenantProfileForm = document.createElement("FORM");
    tenantProfileForm.setAttribute("id", "tenantProfileForm");
    tenantProfileForm.setAttribute("name", "tenantProfileForm");
    tenantProfileForm.setAttribute("method", "post");
    tenantProfileForm.setAttribute("action", "tenants.php");
    document.getElementById("mainBody").appendChild(tenantProfileForm);
    var table = document.createElement("TABLE");
    table.setAttribute("id", "myTable");


    document.getElementById("tenantProfileForm").appendChild(table);

    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Pets?");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y5 = document.createElement("SELECT");
    y5.setAttribute("name", "pets");

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "no");
    var t5 = document.createTextNode("No");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "yes");
    var t5 = document.createTextNode("Yes");
    opt.appendChild(t5);
    y5.appendChild(opt);

    td2.appendChild(y5);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);



    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Smoking?");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y5 = document.createElement("SELECT");
    y5.setAttribute("name", "smoking");

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "no");
    var t5 = document.createTextNode("No");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "yes");
    var t5 = document.createTextNode("Yes");
    opt.appendChild(t5);
    y5.appendChild(opt);

    td2.appendChild(y5);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);

    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Age?");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y2 = document.createElement("INPUT");

    y2.setAttribute("name", "age");

    td2.appendChild(y2);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);


    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Occupation:");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y5 = document.createElement("SELECT");

    y5.setAttribute("name", "occupation");

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "student");
    var t5 = document.createTextNode("Student");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "software");
    var t5 = document.createTextNode("Software");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "accounting");
    var t5 = document.createTextNode("Accounting");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "buisness");
    var t5 = document.createTextNode("Buisness");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "healthcare");
    var t5 = document.createTextNode("Healthcare");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "unemployed");
    var t5 = document.createTextNode("Unemployed");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "other");
    var t5 = document.createTextNode("Other");
    opt.appendChild(t5);
    y5.appendChild(opt);

    td2.appendChild(y5);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);


    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Level of Income:");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y5 = document.createElement("SELECT");
    y5.setAttribute("name", "income");

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "<1000 moth per month");
    var t5 = document.createTextNode("<1000 moth per month");
    opt.appendChild(t5);
    y5.appendChild(opt);
    
    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "1000-2000 per month");
    var t5 = document.createTextNode("1000-2000 per month");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "2000-5000 per month");
    var t5 = document.createTextNode("2000-5000 per month");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "5000-100000 per month");
    var t5 = document.createTextNode("5000-100000 per month");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "10000-20000 per month");
    var t5 = document.createTextNode("10000-20000 per month");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", ">20000 per moth");
    var t5 = document.createTextNode(">20000 per moth");
    opt.appendChild(t5);
    y5.appendChild(opt);

    td2.appendChild(y5);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);

    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    td1.setAttribute("colspan", "2");

    td1.setAttribute("align", "center");

    var y2 = document.createElement("INPUT");

    y2.setAttribute("name", "profileSubmit");

    y2.setAttribute("type", "submit");

    y2.setAttribute("value", "Submit");

    td1.appendChild(y2);

    tr1.appendChild(td1);

    document.getElementById("myTable").appendChild(tr1);
 
}

function setTenantPreferences() {
    document.getElementById("mainBody").innerHTML = "";
    document.getElementById("welcome").innerHTML = "";
    var tenantPreferenceForm = document.createElement("FORM");
    tenantPreferenceForm.setAttribute("id", "tenantPreferenceForm");
    tenantPreferenceForm.setAttribute("name", "tenantPreferenceForm");
    tenantPreferenceForm.setAttribute("method", "post");
    tenantPreferenceForm.setAttribute("action", "tenants.php");
    document.getElementById("mainBody").appendChild(tenantPreferenceForm);

    var table = document.createElement("TABLE");
    table.setAttribute("id", "myTable");

    document.getElementById("tenantPreferenceForm").appendChild(table);

    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    td1.setAttribute("colspan", "2");

    var t2 = document.createTextNode("Location of the Property:");

    td1.appendChild(t2);

    td1.setAttribute("align", "center");

    tr1.appendChild(td1);

    document.getElementById("myTable").appendChild(tr1);


    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("City:");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y2 = document.createElement("INPUT");

    y2.setAttribute("size", "60");

    y2.setAttribute("name", "city");

    td2.appendChild(y2);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);

    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Street:");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y2 = document.createElement("INPUT");

    y2.setAttribute("size", "60");

    y2.setAttribute("name", "street");

    td2.appendChild(y2);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);

    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Province:");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y2 = document.createElement("INPUT");

    y2.setAttribute("size", "60");

    y2.setAttribute("name", "province");

    td2.appendChild(y2);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);


    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Price Range:");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y5 = document.createElement("SELECT");

    y5.setAttribute("name", "pricerange");

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "500");
    var t5 = document.createTextNode("<500 moth per month");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "5001000");
    var t5 = document.createTextNode("500-1000 per month");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "10002000");
    var t5 = document.createTextNode("1000-2000 per month");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "20005000");
    var t5 = document.createTextNode("2000-5000 per month");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "5000100000");
    var t5 = document.createTextNode("5000-100000 per month");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "1000020000");
    var t5 = document.createTextNode("10000-20000 per month");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "20000");
    var t5 = document.createTextNode(">20000 per moth");
    opt.appendChild(t5);
    y5.appendChild(opt);

    td2.appendChild(y5);

    tr1.appendChild(td2);



    document.getElementById("myTable").appendChild(tr1);


    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Availability:");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y5 = document.createElement("SELECT");

    y5.setAttribute("name", "availability");

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "now");
    var t5 = document.createTextNode("Available Now");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "nextmoth");
    var t5 = document.createTextNode("Available Next Month");
    opt.appendChild(t5);
    y5.appendChild(opt);

    var opt = document.createElement("OPTION");
    opt.setAttribute("value", "message");
    var t5 = document.createTextNode("Not Specified");
    opt.appendChild(t5);
    y5.appendChild(opt);

    td2.appendChild(y5);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);

    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    var t2 = document.createTextNode("Message:");

    td1.appendChild(t2);

    tr1.appendChild(td1);

    var td2 = document.createElement("TD");

    var y4 = document.createElement("TEXTAREA");

    y4.setAttribute("rows", "10");
    y4.setAttribute("cols", "50");
    y4.setAttribute("maxlength", "300");
    y4.setAttribute("name", "message");


    td2.appendChild(y4);

    tr1.appendChild(td2);

    document.getElementById("myTable").appendChild(tr1);

    var tr1 = document.createElement("TR");

    var td1 = document.createElement("TD");

    td1.setAttribute("colspan", "2");

    td1.setAttribute("align", "center");

    var y2 = document.createElement("INPUT");

    y2.setAttribute("type", "submit");

    y2.setAttribute("value", "Submit");

    y2.setAttribute("name", "preferenceSubmit");

    td1.appendChild(y2);

    tr1.appendChild(td1);

    document.getElementById("myTable").appendChild(tr1);
}

function slideBanner(){

setInterval(function(){slideBannerFunct()},3000);

}

function slideBannerFunct() {
	
if(document.getElementById("bottom").style.opacity!="0")
{
   document.getElementById("bottom").style.opacity="0";
   document.getElementById("top").style.opacity="0";
document.getElementById("middle").style.opacity="1";
}
else if (document.getElementById("middle").style.opacity=="1")
{
   document.getElementById("middle").style.opacity="0";
   document.getElementById("bottom").style.opacity="0";
document.getElementById("top").style.opacity="1";
}
   else if (document.getElementById("top").style.opacity=="1")
 {
   document.getElementById("top").style.opacity="0";
   document.getElementById("middle").style.opacity="0";
document.getElementById("bottom").style.opacity="1";
}
}





