<!DOCTYPE html>
<html>
<head>
<title>Rental Matching Market - Tenants</title>
<meta name="author" content="Jesus Imery">
<meta name="keywords" content="rent,match,tenant,property,owner,lease">
<link rel="stylesheet" type="text/css" href="rentalmatchingmarket.css">
<script src="rentalmatchingmarket.js"></script>
<?php
session_start();
$login=$_SESSION["sessionlogin"];
$rebuildProfile=false;
$rebuildPreference=false;
$Err = "";

function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

function appendRowWithData($data, $type){
echo '
var tr1 = document.createElement("TR");
var td1 = document.createElement("TD");';
echo 'var datatype = "' . $type . '";';
echo '
var t2 = document.createTextNode(datatype);
td1.appendChild(t2);
tr1.appendChild(td1);
var td1 = document.createElement("TD");
';
echo 'var data = ' . $data . ';
var t2 = document.createTextNode(data);
td1.appendChild(t2);
tr1.appendChild(td1);
document.getElementById("myTable").appendChild(tr1);
';
}

if(isset($_POST['preferenceSubmit']))
{
$dbhost = 'localhost:3306';
$dbuser = 'root';
$dbpass = '';
$dbname = 'rental_7174276';
$conn = new mysqli($dbhost, $dbuser, $dbpass,$dbname);
if(! $conn )
{
  die('Could not connect: ' . mysql_error());
}

if(! get_magic_quotes_gpc() )
{
   $city = addslashes ($_POST['city']);
   $street = addslashes ($_POST['street']);
   $province = addslashes ($_POST['province']);
   $pricerange = addslashes ($_POST['pricerange']);
   $availability = addslashes ($_POST['availability']);
   $message = addslashes ($_POST['message']);
}
else
{
      $city =  ($_POST['city']);
   $street =  ($_POST['street']);
   $province =  ($_POST['province']);
   $pricerange =  ($_POST['pricerange']);
   $availability =  ($_POST['availability']);
   $message =  ($_POST['message']);
}

$result=true;

if (empty($_POST["city"])) {
     $Err = $Err . "City is required. ";
	 $result=false;
   }

   if (empty($_POST["street"])) {
     $Err = $Err .  "Street is required. ";
	 $result=false;
   }

     if (empty($_POST["province"])) {
     $Err = $Err .  "Province is required. ";
	 $result=false;
   }

    if ($result==true)
	{
	


	  $sql = "SELECT id, login FROM tenantspreference WHERE login='" . $login . "'";
		$result = $conn->query($sql);

  if ($result->num_rows > 0) {
    
echo '<script language="javascript">';
echo 'alert("Overwriting preferences!")';
echo '</script>';

	$sql= "DELETE FROM tenantspreference WHERE login='" . $login . "'";
	$retval = mysqli_query( $conn, $sql );
	if(! $retval )
{
  die('Could not enter data: ' . mysql_error());
}
	}



	$sql = "INSERT INTO  tenantspreference".
       "(login,city,street,province,pricerange,availability,message)".
       "VALUES ".
       "('$login','$city','$street','$province','$pricerange','$availability','$message')";
	   mysql_select_db('rental_7174276');
$retval = mysqli_query(  $conn, $sql);
if(! $retval )
{
  die('Could not enter data: ' . mysql_error());
}
echo '<script language="javascript">';
echo 'alert("Data entered succesfully!")';
echo '</script>';
mysqli_close($conn);
goto pag;
}
else
{
$rebuildPreference=true;
goto pag;
}
}
else
{
if (isset($_POST['profileSubmit']))
{
$dbhost = 'localhost:3306';
$dbuser = 'root';
$dbpass = '';
$dbname = 'rental_7174276';
$conn = new mysqli($dbhost, $dbuser, $dbpass,$dbname);
if(! $conn )
{
  die('Could not connect: ' . mysql_error());
}

if(! get_magic_quotes_gpc() )
{
   $pets = addslashes ($_POST['pets']);
   $smoking = addslashes ($_POST['smoking']);
   $age = addslashes ($_POST['age']);
   $occupation = addslashes ($_POST['occupation']);
   $income = addslashes ($_POST['income']);
}
else
{
      $pets =  ($_POST['pets']);
   $smoking =  ($_POST['smoking']);
   $age =  ($_POST['age']);
   $occupation =  ($_POST['occupation']);
   $income =  ($_POST['income']);
}

$result=true;

if (empty($_POST["age"])) {
     $Err = $Err . "Age is required. ";
	 $result=false;
   }

    if ($result==true)
	{
	$login=$_SESSION["sessionlogin"];


	  $sql = "SELECT id, login FROM tenantsprofile WHERE login='" . $login . "'";
	$result = $conn->query($sql);

  if ($result->num_rows > 0) {
    
echo '<script language="javascript">';
echo 'alert("Overwriting profile!")';
echo '</script>';

	$sql= "DELETE FROM tenantsprofile WHERE login='" . $login . "'";
	$retval = mysqli_query( $conn, $sql );
	if(! $retval )
{
  die('Could not enter data: ' . mysql_error());
}
	}



	$sql = "INSERT INTO  tenantsprofile".
       "(login,pets,smoking,age,occupation,income)".
       "VALUES ".
       "('$login','$pets','$smoking','$age','$occupation','$income')";
	   mysql_select_db('rental_7174276');
$retval = mysqli_query(  $conn, $sql);
if(! $retval )
{
  die('Could not enter data: ' . mysql_error());
}
echo '<script language="javascript">';
echo 'alert("Data entered succesfully!")';
echo '</script>';
mysqli_close($conn);
goto pag;
}
else
{
$rebuildProfile=true;
goto pag;
}
}
else
{
pag:
?>
</head>
<body>

<div class="top" id="top">
<h1>Tenant's Menu</h1>
<center>
<table>
<tr>
<th>
<a href='index.html' class='button'>Home</a>
</th>
<th>
<a href='register.php' class='button'>Register</a></th>
<th>
<a href='ownerslogin.php' class='button'>Owners</a></th>
<th>
<a href='tenantslogin.php' class='button'>Tenants</a></th>
</tr>
</table>
</center>
</div>

<div class="left">
<p class='button' onclick="setTenantProfile()">Set Profile</></p>
    <p class='button' onclick="setTenantPreferences()">Set Rental Preferences</></p>
	    <p class='button' onclick='location.href="?viewProfile=1"'>View Profile</></p>
			    <p class='button' onclick='location.href="?viewPreference=1"'>View Rental Preferences</></p>


</div>
 
    <div class="main">
        <center>
		<p id="welcome">Welcome <?php echo $_SESSION["sessionfname"] . "!"; ?></p>
		<p id="mainBody">Select an option to start</p>
	<span class="error"><?php echo $Err;?></span>
		</center>
              </div>
     <footer class="fixed">
<p>Copyright: Imaginary INC.</p>
<p><a href="mailto:yisas_0929@hotmail.com">Contact: yisas_0929@hotmail.com</a></p>
</footer>
<?php
if ($rebuildPreference==true)
{
  echo '<script language="javascript"> setTenantPreferences();  </script>';
}
if ($rebuildProfile==true)
{
  echo '<script language="javascript"> setTenantProfile();  </script>';
}
}
}
if(isset ($_GET['viewProfile']))
{
$dbhost = 'localhost:3306';
$dbuser = 'root';
$dbpass = '';
$dbname = 'rental_7174276';
$conn = new mysqli($dbhost, $dbuser, $dbpass,$dbname);
if(! $conn )
{
  die('Could not connect: ' . mysql_error());
}
 $sql = "SELECT * FROM tenantsprofile WHERE login='" . $login . "'";
	mysql_select_db('rental_7174276');
	$result = $conn->query($sql);
  if ($result->num_rows == 0) {
echo '<script language="javascript">';
echo 'alert("No profile yet!")';
echo '</script>';
$result2=false;
}
else
{
$row=$result->fetch_assoc();
 $pets = json_encode($row["pets"]);
  $smoking = json_encode($row["smoking"]);
    $age = json_encode($row["age"]);
	    $occupation = json_encode($row["occupation"]);
			    $income = json_encode($row["income"]);



echo '<script language="javascript">';
echo '
document.getElementById("mainBody").innerHTML = "";
document.getElementById("welcome").innerHTML = "";
var table = document.createElement("TABLE");
table.setAttribute("id", "myTable");
table.setAttribute("class","backgroundBox");
document.getElementById("mainBody").appendChild(table);
';
 $message="Pets:";
echo ''. appendRowWithData($pets,$message) .'
';
 $message="Smoking:";
echo ''. appendRowWithData($smoking,$message) .'
';
 $message="Age:";
echo ''. appendRowWithData($age,$message) .'
';
 $message="Occupation:";
echo ''. appendRowWithData($occupation,$message) .'
';
 $message="Income:";
echo ''. appendRowWithData($income,$message) .'
</script>';
}
}

if(isset ($_GET['viewPreference']))
{
$dbhost = 'localhost:3306';
$dbuser = 'root';
$dbpass = '';
$dbname = 'rental_7174276';
$conn = new mysqli($dbhost, $dbuser, $dbpass,$dbname);
if(! $conn )
{
  die('Could not connect: ' . mysql_error());
}
 $sql = "SELECT * FROM tenantspreference WHERE login='" . $login . "'";
	mysql_select_db('rental_7174276');
	$result = $conn->query($sql);
  if ($result->num_rows == 0) {
echo '<script language="javascript">';
echo 'alert("No preferences yet!")';
echo '</script>';
$result2=false;
}
else
{
$row=$result->fetch_assoc();
 $city = json_encode($row["city"]);
  $street = json_encode($row["street"]);
    $province = json_encode($row["province"]);
	    $pricerange = json_encode($row["pricerange"]);
			    $availability = json_encode($row["availability"]);
							    $message = json_encode($row["message"]);




echo '<script languprovince="javascript">';
echo '
document.getElementById("mainBody").innerHTML = "";
document.getElementById("welcome").innerHTML = "";
var table = document.createElement("TABLE");
table.setAttribute("id", "myTable");
table.setAttribute("class","backgroundBox");
document.getElementById("mainBody").appendChild(table);
';
 $messprovince="City:";
echo ''. appendRowWithData($city,$messprovince) .'
';
 $messprovince="Street:";
echo ''. appendRowWithData($street,$messprovince) .'
';
 $messprovince="Province:";
echo ''. appendRowWithData($province,$messprovince) .'
';
 $messprovince="Price Range:";
echo ''. appendRowWithData($pricerange,$messprovince) .'
';
 $messprovince="Availability:";
echo ''. appendRowWithData($availability,$messprovince) .'
';
 $messprovince="Message:";
echo ''. appendRowWithData($message,$messprovince) .'
</script>';
}
}
?>
</body>
</html>

