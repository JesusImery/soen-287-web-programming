<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
<title>Rental Matching Market - Tenants</title>
<meta name="author" content="Jesus Imery">
<meta name="keywords" content="rent,match,tenant,property,owner,lease">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="rentalmatchingmarket.css">
<script src="rentalmatchingmarket.js"></script>
<?php
function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

//Initialize error messages.
$passErr = $loginErr = "";

if(isset($_POST['loginsubmit']))
{
$dbhost = 'localhost:3306';
$dbuser = 'root';
$dbpass = '';
$dbname = 'rental_7174276';
$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if(! get_magic_quotes_gpc() )
{
   $login = addslashes ($_POST['login']);
   $password = addslashes ($_POST['loginpassword']);
}
else
{
   $login =  ($_POST['login']);
   $password =  ($_POST['loginpassword']);
}

$result=true;

    if (empty($_POST["login"])) {
     $loginErr = "Login is required. ";
	 $result=false;
   } else {
     $test_fname = test_input($_POST["login"]);
     if (!preg_match("/^[A-Za-z0-9]{6}[A-Za-z0-9]*$/",$test_fname)) {
       $loginErr = "Wrong login input. Letters and digits only and must be at least 6 characters long. "; 
	   $result=false;
     }
   }

 

      if (empty($_POST["loginpassword"])) {
     $passErr = "Password is required. ";
	 $result=false;
   } else {
     $test_fname = test_input($_POST["loginpassword"]);
     if (!preg_match("/^.*(?=.{6,})(?=.*\d)(?=.*[a-zA-Z]).*$/",$test_fname)) {
       $passErr = "Wrong password input. Letters and digits only and must be at least 6 characters long, cointaining at least one of each. "; 
	   $result=false;
     }
   }


   if ($result==true)
   {
  $sql = "SELECT id, fname,sname,phone,email,login,password FROM tenantsregistration WHERE login='" . $_POST["login"] . "' AND password='" . $_POST["loginpassword"] . "'";
  $result = $conn->query($sql);

  if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "ID: " . $row["id"]. "First Name: " . $row["fname"]. " Last Name: " . $row["sname"].  "<br>";
		$_SESSION["sessionid"] = $row["id"];
		$_SESSION["sessionfname"] = $row["fname"];
		$_SESSION["sessionsname"] = $row["sname"];
		$_SESSION["sessionphone"] = $row["phone"];
		$_SESSION["sessionemail"] = $row["email"];
		$_SESSION["sessionlogin"] = $row["login"];
		$_SESSION["sessionpassword"] = $row["password"];
		header("Location:tenants.php");
    }
} else {
   echo '<script language="javascript">';
echo 'alert("Invalid user.")';
echo '</script>';
goto pag;
}
$conn->close();


}
else
{
goto pag;
}
}
else
{
pag:
?>

</head>
<body>
<body>
<img class="banner" id="top" style="background-image:url(banner2.jpg);">
<img class="banner" id="middle" style="background-image:url(banner.jpg);">
<img class="banner" id="bottom" style="background-image:url(banner3.jpg);">
<script>
slideBanner();
</script>
  <!-- Bootstrap code based on TA's submission, used with here permission -->
	<header>
	 <nav class="navbar navbar-default navbar-fixed-top">
	  <div class="container-fluid">
		<div class="navbar-header">
			<button class="navbar-toggle collapsed" aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" type="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>		
		    <a class="navbar-brand" href="#">Tenant's Login</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
		  <ul class="nav navbar-nav">
			<li><a href="./index.html">Home</a></li>
			<li><a href="./register.php">Register</a></li>
			<li><a href="./ownerslogin.php">Owners</a></li>
			<li><a href="./tenantslogin.php">Tenants</a></li>		
		  </ul>
		</div>
	  </div>
	 </nav>
	</header>
<p class="error">* Required Field</p>
<p>Sign In:</p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
<table>
<tr>
<th>
Login Name: 
</th>
<th>
<input type="text" name="login" id="login">
</th>
<th>
<span class="error">* <?php echo $loginErr;?></span>
</th>
</tr>
<tr>
<th>
Password:
</th>
<th>
<input type="password" name="loginpassword" id="loginpassword">
</th>
<th>
<span class="error">* <?php echo $passErr;?></span>
</th>
</tr>
</table>
<input type="submit" value="Submit" name="loginsubmit" id="loginsubmit">
</form>
    <p id="responsepar"></p>
<footer class="footer">
<p>Copyright: Imaginary INC.<br />
<a href="mailto:yisas_0929@hotmail.com">Contact: yisas_0929@hotmail.com</a></p>
</footer>
<?php
}
?>
</body>
</html>
