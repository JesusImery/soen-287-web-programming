<!DOCTYPE html>
<html>
<head>
<title>Rental Matching Market - Register</title>
<meta name="author" content="Jesus Imery">
<meta name="keywords" content="rent,match,tenant,property,owner,lease">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="rentalmatchingmarket.css">
<script src="rentalmatchingmarket.js"></script>
<?php

function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

//Initialize error messages.
$equalPassErr = $confirmErr = $passErr = $loginErr = $emailErr = $pErr = $fnameErr = $snameErr="";

if(isset($_POST['register']))
{
$dbhost = 'localhost:3306';
$dbuser = 'root';
$dbpass = '';
$dbname='rental_7174276';
$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
if(! $conn )
{
  die('Could not connect: ' . mysql_error());
}

if(! get_magic_quotes_gpc() )
{
   $fName = addslashes ($_POST['firstname']);
   $lName = addslashes ($_POST['secondname']);
   $phone = addslashes ($_POST['phonenumber']);
   $email = addslashes ($_POST['emailaddress']);
   $login = addslashes ($_POST['login']);
   $password = addslashes ($_POST['loginpassword']);
}
else
{
      $fName =  ($_POST['firstname']);
   $lName =  ($_POST['secondname']);
   $phone =  ($_POST['phonenumber']);
   $email =  ($_POST['emailaddress']);
   $login =  ($_POST['login']);
   $password =  ($_POST['loginpassword']);
}

$result=true;

if (empty($_POST["firstname"])) {
     $fnameErr = "First name is required. ";
	 $result=false;
   } else {
     $test_fname = test_input($_POST["firstname"]);
     // check if name only contains letters and hyphen
     if (!preg_match("/^[A-Za-z\-]+$/",$test_fname)) {
       $fnameErr = "Only letters and hyphen allowed for first name. "; 
	   $result=false;
     }
   }
   if (empty($_POST["secondname"])) {
     $snameErr = "Second name is required. ";
	 $result=false;
   } else {
     $test_name = test_input($_POST["secondname"]);
     // check if name only contains letters and hyphen
     if (!preg_match("/^[A-Za-z\-]+$/",$test_name)) {
       $snameErr = "Only letters and hyphen allowed for second name. "; 
	   $result=false;
     }
   }

    if (empty($_POST["phonenumber"])) {
   } else {
     $test_name = test_input($_POST["phonenumber"]);
     
     if (!preg_match("/^\([0-9]{3}\)[0-9]{3}\-[0-9]{4}$/",$test_name)) {
       $pErr = "Wrong Phone Number input "; 
	   $result=false;
     }
   }

     if (empty($_POST["emailaddress"])) {
     $emailErr = "Email is required. ";
	 $result=false;
   } else {
     $test_fname = test_input($_POST["emailaddress"]);
     if (!preg_match("/^[A-Za-z0-9_]+@[A-Za-z0-9]+\.com$/",$test_fname)) {
       $emailErr = "Wrong email input. "; 
	   $result=false;
     }
   }

    if (empty($_POST["login"])) {
     $loginErr = "Login is required. ";
	 $result=false;
   } else {
     $test_fname = test_input($_POST["login"]);
     if (!preg_match("/^[A-Za-z0-9]{6}[A-Za-z0-9]*$/",$test_fname)) {
       $loginErr = "Wrong login input. Letters and digits only and must be at least 6 characters long. "; 
	   $result=false;
     }
   }

   $twoPass=true; //Controls whether both passes are valid to compare them.

      if (empty($_POST["loginpassword"])) {
     $passErr = "Password is required. ";
	 $result=false;
	 $twoPass=false;
   } else {
     $test_fname = test_input($_POST["loginpassword"]);
     if (!preg_match("/^.*(?=.{6,})(?=.*\d)(?=.*[a-zA-Z]).*$/",$test_fname)) {
       $passErr = "Wrong password input. Letters and digits only and must be at least 6 characters long, cointaining at least one of each. "; 
	   $result=false;
	   $twoPass=false;
     }
   }

    if (empty($_POST["confirmpassword"])) {
     $confirmErr = "Password is required. ";
	 $result=false;
	 $twoPass=false;
   } else {
     $test_fname = test_input($_POST["confirmpassword"]);
     if (!preg_match("/^.*(?=.{6,})(?=.*\d)(?=.*[a-zA-Z]).*$/",$test_fname)) {
       $confirmErr = "Wrong password input. Letters and digits only and must be at least 6 characters long, cointaining at least one of each. "; 
	   $result=false;
	   $twoPass=false;
     }
   }

  if ($twoPass==true)
 {
 if ($_POST["confirmpassword"]!=$_POST["loginpassword"])
 {
 $equalPassErr="Passwords must match.";
 $result=false;
 }
 }


   if ($result==true)
   {
   $result2=true;
   if ($_POST["usertype"]=="tenat")
   {
	   
	    $sql = "SELECT id, login FROM tenantsregistration WHERE login='" . $login . "'";
	mysql_select_db('rental_7174276');
	$result = $conn->query($sql);

  if ($result->num_rows > 0) {
    

echo '<script language="javascript">';
echo 'alert("Login already exists!")';
echo '</script>';
$result2=false;
}
else
{
	    
$sql = "INSERT INTO  tenantsregistration".
       "(fname,sname,phone,email,login,password) ".
       "VALUES ".
       "('$fName','$lName','$phone','$email','$login','$password')"; 
}  
}
	   else if ($_POST["usertype"]=="owner")
	   {
		   
		    $sql = "SELECT id, login FROM ownersregistration WHERE login='" . $login . "'";
	mysql_select_db('rental_7174276');
	$result = $conn->query($sql);
	 if ($result->num_rows > 0) {
    

echo '<script language="javascript">';
echo 'alert("Login already exists!")';
echo '</script>';
$result2=false;
}
else
{
		   
	   $sql = "INSERT INTO  ownersregistration".
       "(fname,sname,phone,email,login,password) ".
       "VALUES ".
       "('$fName','$lName','$phone','$email','$login','$password')";
}
	   }
	   if ($result2==true)
	   {
mysql_select_db('rental_7174276');
	$retval = $conn->query($sql);
if(! $retval )
{
  die('Could not enter data: ' . mysql_error());
}
echo '<script language="javascript">';
echo 'alert("Registration successful!")';
echo '</script>';
mysqli_close($conn);
}

    echo "<SCRIPT type='text/javascript'> 
        window.location.replace(\"index.html\");
    </SCRIPT>";
}
else
{
goto pag;
}
}
else
{
pag:
?>

</head>
<body>
<img class="banner" id="top" style="background-image:url(banner2.jpg);">
<img class="banner" id="middle" style="background-image:url(banner.jpg);">
<img class="banner" id="bottom" style="background-image:url(banner3.jpg);">
<script>
slideBanner();
</script>
  <!-- Bootstrap code based on TA's submission, used with here permission -->
	<header>
	 <nav class="navbar navbar-default navbar-fixed-top">
	  <div class="container-fluid">
		<div class="navbar-header">
			<button class="navbar-toggle collapsed" aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" type="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>		
		    <a class="navbar-brand" href="#">Registration</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
		  <ul class="nav navbar-nav">
			<li><a href="./index.html">Home</a></li>
			<li><a href="./register.php">Register</a></li>
			<li><a href="./ownerslogin.php">Owners</a></li>
			<li><a href="./tenantslogin.php">Tenants</a></li>		
		  </ul>
		</div>
	  </div>
	 </nav>
	</header>
<p class="error">* Required Field</p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
 


<table class="backgroundBox">
<tr>
<th>
First Name: 
</th>
<th>
<input type="text" name="firstname" id="firstname">
</th>
<th>
<span class="error">* <?php echo $fnameErr;?></span>
</th>
</tr>
<tr>
<th>
Second Name:
</th>
<th>
<input type="text" name="secondname" id="secondname">
</th>
<th>
<span class="error">* <?php echo $snameErr;?></span>
</th>
</tr>
<tr>
<th>
Registered As:
</th>
<th>
<select name="usertype" id="usertype">
<option value="tenat">Tenant</option>
<option value="owner">Property Owner</option>
</select>
</th>
</tr>
<tr>
<th>
Phone Number:
</th>
<th>
<input type="text" name="phonenumber" id="phonenumber">
</th>
    <th>
        (xxx)xxx-xxxx
    </th>
	<th>
<span class="error"><?php echo $pErr;?></span>
</th>
</tr>
<tr>
<th>
Email Address: 
</th>
<th>
<input type="text" name="emailaddress" id="emailaddress">
</th>
<th>
<span class="error">* <?php echo $emailErr;?></span>
</th>
</tr>
<tr>
<th>
Login Name: 
</th>
<th>
<input type="text" name="login" id="login">
</th>
<th>
<span class="error">* <?php echo $loginErr;?></span>
</th>
</tr>
<tr>
<th>
Login Password:
</th>
<th>
<input type="password" name="loginpassword" id="loginpassword">
</th>
<th>
<span class="error">* <?php echo $passErr;?></span>
</th>
</tr>
<tr>
<th>
Confirm Password:
</th>
<th>
<input type="password" name="confirmpassword" id="confirmpassword">
</th>
<th>
<span class="error">* <?php echo $confirmErr;?></span>
</th>
</tr>
<tr>
<th>
<span class="error"><?php echo $equalPassErr;?></span>
</th>
</tr>
</table>
<input type="submit" value="Submit" name="register" id="register">
</form>
<p id="responsepar"></p>
<footer class="footer">
<p>Copyright: Imaginary INC.<br />
<a href="mailto:yisas_0929@hotmail.com">Contact: yisas_0929@hotmail.com</a></p>
</footer>
<?php
}
?>
</body>
</html>
